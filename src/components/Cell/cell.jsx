import React from 'react';
import ReactDOM from 'react-dom';
import './cell.scss';
import { connect } from 'react-redux';

class Cell extends React.Component {
	constructor(props, context) {
		super(props);
		this.store = props.store;
		this.state = {};
		this.onPlay = this.onPlay.bind(this);
		this.wait = 300;
		this.init();
	}
	init() {
		let r = parseInt(this.props.rowIndex, 10);
		let c = parseInt(this.props.cellIndex, 10);
		this.index = c + 3 * r;
	}
	getMoves() {
		return this.props.store.moves;
	}
	getMove() {
		let move = this.getMoves().filter(move => {
			return move[1] === this.index;
		});
		return move.length? move[0]: null;
	}
	onPlay() {
		if (this.props.store.winner) return;
		if (this.props.store.turn !== 1 || this.state.occupied) {
			return console.log('illegal move');
		}
		this.props.dispatch({
			type: 'MOVE',
			payload: [1, this.index]
		});
	}
	render() {
		var content = '', move = null, win = '', occupied = '';
		let state = this.props.store;

		if (move = this.getMove()) {
			this.state.occupied = !!move;
			occupied = move? 'occupied': '';
			content = move[0]? state.pSymbol: state.cSymbol;
			let winner = state.winner;
			if (winner && winner.combo.indexOf(this.index)>-1) {
				win = winner.player? 'win ': 'lose ';
			}
		} else this.state.occupied = false;

		return <span className={'cell '+win+occupied} onClick={this.onPlay}>{content}</span>
	}
}
/*const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(Actions, dispatch)
});*/


const mapStateToProps = state => ({store: state});
export default connect(mapStateToProps)(Cell);
