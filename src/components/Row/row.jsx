import React from 'react';
import ReactDOM from 'react-dom';
import Cell from '../Cell/cell.jsx';
import './row.scss';

class Row extends React.Component {

	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div className='row'>
				<Cell rowIndex={this.props.rowIndex} cellIndex='0'/>
				<Cell rowIndex={this.props.rowIndex} cellIndex='1'/>
				<Cell rowIndex={this.props.rowIndex} cellIndex='2'/>
			</div>
		);
	}
}
export default Row;