import React from 'react';
import ReactDOM from 'react-dom';
import './modal.scss';
import { connect } from 'react-redux';

class Modal extends React.Component {

	constructor(props) {
		super(props);
		this.state = {ready: false};
		this.wait = 300;
	}
	componentWillUpdate(nextProps, nextState) {
		if ((nextProps.winner || nextProps.isEnd) && !nextState.ready) {
			setTimeout(() => {
				this.setState({ready: true});
			}, this.wait);
		}
	}
	render() {
		if ((!this.state.ready && !this.props.isStart) || this.props.isStart) return null;
		let msgs = [
			'Boooo!',
			'Yaaay!',
			'Draw, ugh.'
		],
			winClass = [
				'',
				' win',
				' draw'
			],
			winner = this.props.winner,
			textIndex = winner? winner.player: 2,
			text = msgs[textIndex],
			win = winClass[textIndex];
		this.state.ready = false;
		return (
			<div className={'modal'}>
				<div className='back' />
				<div className = {'front' + win}>
					<h1>{text}</h1>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	winner: state.winner,
	isStart: state.moves.length === 0,
	isEnd: state.moves.length >= 9
});
export default connect(mapStateToProps)(Modal);
