import React from 'react';
import Row from '../Row/row.jsx';
import Modal from '../Modal/modal.jsx';
import './App.scss';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

class App extends React.Component {

	constructor(props) {
		super(props);
		this.store = props.state;
		this.onRestart = this.onRestart.bind(this);
		this.end = '';
		this.forceRefresh = false;
	}
	componentWillUpdate() {
		let winner = this.store.winner;
		if (!winner && !this.forceRefresh) return;
		this.forceRefresh = false;
		this.end = winner? 'end': '';
	}
	onRestart() {
		this.forceRefresh = true;
		this.props.dispatch({
			type: 'RESTART'
		});
	}
	render() {
		return (
			<div className = 'container'>
				<h1>TicTacToe</h1>
				<button onClick={this.onRestart}>Restart</button>
				<div className={'table ' + this.end}>
					<Row rowIndex='0'/>
					<Row rowIndex='1'/>
					<Row rowIndex='2'/>
					<Modal />
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({state: state});
export default connect(mapStateToProps)(App);