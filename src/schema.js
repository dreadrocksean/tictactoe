import * as _ from 'underscore';
import PostsList from './data/posts';
import AuthorsList from './data/authors';
import {CommentList, ReplyList} from './data/comments';

import {
	// These are the basic GraphQL types
	GraphQLInt,
	GraphQLFloat,
	GraphQLString,
	GraphQLList,
	GraphQLObjectType,
	GraphQLEnumType,// This is used to create required fields and arguments
	GraphQLNonNull,// This is the class we need to create the schema
	GraphQLSchema,// This function is used execute GraphQL queries
	graphql
} from 'graphql';

// This is the Root Query
const Query = new GraphQLObjectType({
	name: 'BlogSchema',
	description: 'Root of the Blog Schema',
	fields: () => ({
		plays: {
			type: new GraphQLList(Play),
			args: {
				index: {type: GraphQLInt,
				value: {type: GraphQLInt}
			},
			resolve: function(root, args) {
				if (Object.keys(args).length === 0) return PostsList;
				return PostsList.filter(p => {
					return p._id === args.id || p.author === args.author;
				});
			}
		},
		authors: {
			type: new GraphQLList(Author),
			args: {
				name: {type: GraphQLString}
			},
			resolve: function(root, {name}) {
				return name? AuthorsList.filter(a => a.name === name): AuthorsList;
			}
		},
		echo: {
			type: GraphQLString,
			description: 'Echo what you enter',
			args: {
				message: {type: GraphQLString}
			},
			resolve: function(source, {message}) {
				return {aa: 10};
			}
		}
	})
});

const Post = new GraphQLObjectType({
	name: 'Post',
	description: 'This is a Post',
	fields: () => ({
		_id: {type: new GraphQLNonNull(GraphQLString)},
		title: {
			type: new GraphQLNonNull(GraphQLString),
			resolve: function(post) {
				return post.title || 'Uhhh - learn to spell.'
			}
		},
		author: {
			type: Author,
			resolve: function(post) {
				return AuthorsList.find(a => a._id === post.author);
			}
		},
		category: {
			type: GraphQLString,
			resolve: post => post.category.toUpperCase() || ''
		},
		content: {
			type: GraphQLString,
			resolve: post => post.content.substring(20, 0)
		}
	})
});

const Author = new GraphQLObjectType({
	name: 'Author',
	description: 'This is an Author',
	fields: () => ({
		_id: {type: GraphQLString},
		name: {type: GraphQLString}
	})
});

const Mutation = new GraphQLObjectType({
	name: "BlogMutations",
	description: "Mutations of our blog",
	fields: () => ({
		createPost: {
			type: Post,
			args: {
				title: {type: new GraphQLNonNull(GraphQLString)},
				author: {type: new GraphQLNonNull(GraphQLString)},
				content: {type: new GraphQLNonNull(GraphQLString)}
			},
			resolve: function(source, args) {
				let post = Object.assign({}, args);
				post._id = `${Date.now()}::${Math.ceil(Math.random() * 9999999)}`;
				PostsList.push(post);
				return post;
			}
		}
	})
});

// The Schema
const Schema = new GraphQLSchema({
	query: Query,
	mutation: Mutation
});

const getPosts = function() {

};

let query = `
	query getPosts($author: String) {
		posts(author: $author) {
			_id
			title
			author {
				_id
				name
			}
			category
			content
		}
	}
`;

graphql(Schema, query, null, {author: "indi"}).then(function(result) {
	console.log(result);
});

export default Schema;
