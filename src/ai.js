import Utils from './utils.js';

export default class AI {
	constructor(store) {
		this.store = store;
		this.utils = new Utils();
		this.getPlay = this.getPlay.bind(this);
		this.getMoves = this.getMoves.bind(this);
		this.wait = 300;

		store.subscribe(() => {
			if (
				this.store.getState().winner ||
				this.store.getState().turn !== 0
			) return;
			setTimeout(() => {
				store.dispatch({
					type: 'MOVE',
					payload: [0, this.getPlay()]
				});
			}, this.wait);
		});
	}

	getMoves() {
		return this.store.getState().moves;
	}

	getPlay() {
		var play;
		if ((play = this.getWinMove()) !== null) return play;
		if ((play = this.getBlockMove()) !== null) return play;
		play = this.utils.getRandomSquare(this.getMoves());
		while (this.utils.isOccupied(play, 0, this.getMoves())) {
			console.log('illegal computer move');
			play = this.utils.getRandomSquare(this.getMoves());
		}
		return play;
	}

	fill(tuple) {
		var play;
		let diff = this.utils.getMoveDiff(tuple[0], tuple[1]),
			lower = this.utils.getLowerMove(tuple[0], tuple[1]),
			higher = this.utils.getHigherMove(tuple[0], tuple[1]);
		if (this.utils.isHorizontal(tuple[0], tuple[1])) {
			let rank = Math.floor(lower[1] / 3);
			return (lower[1] + 3 - diff) % 3 + (3 * rank);
		} else if (this.utils.isVertical(tuple[0], tuple[1])) {
			return (lower[1] + 9 - diff) % 9;
		} else {
			if (lower[1] % 3 > higher[1] % 3) { //slopes to the left
				return (lower[1] + 6 - diff) % 6 || 6;
			}
			return (lower[1] + 12 - diff) % 12;
		}
	}

	getBlockMove() {
		let threat = this.getAlertTuple(1);
		if (threat) {
			console.log('threat', threat);
			return threat[1];
		}
		return null;
	}

	getWinMove() {
		let opportunity = this.getAlertTuple(0);
		if (opportunity) {
			console.log('opportunity', opportunity);
			return opportunity[1];
		}
		return null;
	}

	getAlertTuple(player) {
		var alert, fill;
		let moves = this.utils.filterMovesByPlayer(player, this.getMoves());
		for (var i = 0, l = moves.length; i < l; i++) {
			for (var j = i + 1; j < l && j !== i; j++) {
				if (
					this.utils.isHorizontal(moves[i], moves[j]) ||
					this.utils.isVertical(moves[i], moves[j]) ||
					this.utils.isDiagonal(moves[i], moves[j])
				) {
					alert = [moves[i], moves[j]];
					if (this.utils.isOccupied(fill = this.fill(alert), null, this.getMoves())) continue;
					else return [alert, fill];
				}
			}
		}
		return null;
	}

}