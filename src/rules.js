import Utils from './utils.js'

let instance = null;

class Rules {
	constructor() {
		this.utils = new Utils;
		this.isLegal = this.isLegal.bind(this);

		instance = instance || this;
		return instance;
	}
	isTurn(player, starter, moves) {
		return (!moves.length && starter === player) ||
			moves.length && moves[moves.length-1][0] !== player;
	}
	isWon(moves) {
		var winner = null;
		let lowest = this.utils.getLowest(moves);

		if (!this.utils.inRow(0, lowest) &&
			!this.utils.inCol(0, lowest)) return null;

		for (var i = 0, m; m = moves[i]; i++) {
			if (this.utils.inRow(0, m[1]) || this.utils.inCol(0, m[1])) {
				winner = this.checkForWin(m[1], moves);
				if (winner !== null) return winner;
			}
			else continue;
		}
		return winner;
	}

	isLegal(player, move, starter, moves) {
		return !this.isEnd(moves) &&
			this.isTurn(player, starter, moves) &&
			!this.utils.isOccupied(move, player, moves);
	}

	getTurn(starter, moves) {
		let l = moves.length;
		return l >= 9? null: (starter + (l % 2)) % 2;
	}
	isEnd(moves) {
		return moves.length >= 9;
	}
	checkForWin(play, moves) {
		var winner = null;
		if (play === 0) winner = this.whoHStrike(play, moves) || this.whoDRStrike(play, moves) || this.whoVStrike(play, moves);
		else if (play === 2) winner = this.whoVStrike(play, moves) || this.whoDLStrike(play, moves);
		else if (this.utils.inRow(0, play)) winner = this.whoVStrike(play, moves);
		else winner = this.whoHStrike(play, moves);
		return winner;
	}
	whoHStrike(play, moves) {
		let player = this.utils.whoIs(play, moves);
		if (this.utils.isOccupied(play + 1, player, moves)
			&& this.utils.isOccupied(play + 2, player, moves)) {
			return {player: player, combo: [play, play+1, play+2]};
		}
		return null;
	}
	whoVStrike(play, moves) {
		let player = this.utils.whoIs(play, moves);
		if (this.utils.isOccupied(play + 3, player, moves) &&
			this.utils.isOccupied(play + 6, player, moves)) {
			return {player: player, combo: [play, play+3, play+6]};
		}
		return null;
	}
	whoDRStrike(play, moves) {
		let player = this.utils.whoIs(play, moves);
		if (this.utils.isOccupied(play + 4, player, moves) &&
			this.utils.isOccupied(play + 8, player, moves)) {
			return {player: player, combo: [play, play+4, play+8]};
		}
		return null;
	}
	whoDLStrike(play, moves) {
		let player = this.utils.whoIs(play, moves);
		if (this.utils.isOccupied(play + 2, player, moves) &&
			this.utils.isOccupied(play + 4, player, moves)) {
			return {player: player, combo: [play, play+2, play+4]};
		}
		return null;
	}

}

export default Rules;