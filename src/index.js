import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/app.jsx';
import { Provider, connect } from 'react-redux';
import {createStore} from 'redux';
import AI from './ai.js';
import Rules from './rules.js';

const reducer = function(state, action) {
	switch (action.type) {
		case 'MOVE': {
			if (state.winner !== null) return;
			let newState = {...state, moves: [...state.moves, action.payload]};
			newState.turn = rules.getTurn(newState.starter, newState.moves);
			newState.winner = rules.isWon(newState.moves);
			return newState;
		}
		case 'RESTART': {
			let starter = 1-state.starter;
			return {...defaultState, starter: starter, turn: starter};
		}
		default: return state;
	}
}
const defaultState = {
	moves: [],
	cSymbol: 'O',
	pSymbol: 'X',
	starter: 1,
	winner: null,
	turn: 1
};
const store = createStore(reducer, {...defaultState});
console.log('store created', store.getState());
const rules = new Rules();
new AI(store);

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>
	, document.getElementById('app')
);
