class Utils {

	whoIs(play, moves) {
		var move;
		return (move = moves.filter(e => e[1] === play)[0])? move[0]: null;
	}

	isOccupied(play, player, moves) {
		if (player !== 0) {
			player = player || this.whoIs(play, moves);
		}
		let occupied = moves.filter(item => {
			return item[1] === play;
		});
		return occupied.length && occupied[0][0] === player;
	}
	filterMovesByPlayer(player, moves) {
		return moves.filter(e => {
			return e[0] === player;
		});
	}
	getAvailableSquares(moves) {
		let squares =  [0,1,2,3,4,5,6,7,8].filter(function(e) {
			return this.indexOf(e)<0;
		}, moves.map(e => {
			return e[1];
		}));
		return squares;
	}
	getLowest(moves) {
		return Math.min(...moves.map(e => e[1]));
	}
	getRandomSquare(moves) {
		let possibleSquares = this.getAvailableSquares(moves);
		let l = possibleSquares.length;
		let random = this.getRandomInt(0, l);
		return possibleSquares[random];
	}
	////////////////////////////////////////////

	isHorizontal(move1, move2) {
		if (move1[0] !== move2[0]) return false;
		var mod = null;
		if ((mod = this.getLowerMove(move1, move2)[1] % 3) > 1) return false;
		let diff = this.getMoveDiff(move1, move2);
		return diff < 3 - mod;
	}
	isAdjacentHorizontally(move1, move2) {
		return this.isHorizontal(move1, move2) &&
			this.getMoveDiff(move1, move2) === 1;
	}
	isVertical(move1, move2) {
		if (move1[0] !== move2[0]) return false;
		if (this.getLowerMove(move1, move2)[1] > 5) return false;
		return this.getMoveDiff(move1, move2) % 3 === 0;
	}
	isAdjacentVertically(move1, move2) {
		return this.isVertical(move1, move2) &&
			this.getMoveDiff(move1, move2) === 3;
	}
	isDiagonal(move1, move2) {
		if (move1[0] !== move2[0]) return false;
		if (move1[1] % 2 > 0 || move2[1] % 2 > 0) return false;
		let diff = this.getMoveDiff(move1, move2);
		return (diff % 2 === 0 && !this.inSameRow(move1, move2)) ||
			diff % 4 === 0 ||
			move1[1] === 4 ||
			move2[1] == 4;
	}
	isAdjacentDiagonally(move1, move2) {
		return (this.isDiagonal(move1, move2)) &&
			(move1[1] === 4 || move2[1] === 4);
	}
	inSameRow(move1, move2) {
		return Math.floor(move1[1] / 3) === Math.floor(move2[1] / 3)
	}
	inRow(row, play) {
		return Math.floor(play / 3) === row;
	}
	inCol(col, play) {
		return play % 3 === col;
	}

	getHigherMove(move1, move2) {
		if (move1[1] > move2[1]) return move1;
		return move2;
	}
	getLowerMove(move1, move2) {
		if (move1[1] < move2[1]) return move1;
		return move2;
	}
	getMoveDiff(move1, move2) {
		return Math.abs(move2[1] - move1[1]);
	}

	getRandomInt(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min)) + min;
	}

}

export default Utils;