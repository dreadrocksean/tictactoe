module.exports = {
	entry: './src/index.js',
	output: { path: __dirname, filename: 'dist/bundle.js' },
devServer: {
	// inline: true,
	port: 5000
},
	module: {
		loaders: [
			{
				test: /.jsx?$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ['es2015', 'react', 'stage-0']
				}
			},
			{ 
				test: /\.css$/, 
				loader: "style-loader!css-loader" 
			},
			{
				test: /\.scss$/,
				loaders: ['style', 'css', 'sass']
			},
			{
				test: /\.less$/,
				loaders: ['style', 'css', 'less']
			}
		],
	},
	postcss: function () {
		return [autoprefixer, precss];
	} 
};